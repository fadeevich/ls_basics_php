<!doctype html>
<html>
<head>
    <style>
        table {
            border-collapse: collapse;
            border: solid 1px #000;
        }
        td {
            text-align: center;
            width: 50px;
            border: solid 1px #000;
        }
    </style>
    <title>Exercise 07</title>
</head>
<body>
    <table>
        <tr>
            <td></td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
            <td>5</td>
        </tr>
<?php
    for($a=1; $a<=4; $a++){
        echo '<tr>';
        echo "<td>$a</td>";
        for($b=1; $b<=5; $b++){
            $result = $a * $b;
            echo "<td>$result</td>";
        }
        echo '</tr>';
}
?>
    </table>
    <br>
 <!-- Задача. Используя цикл for выведите таблицу квадратов чисел от 0 до 99 -->
    <table>
        <tr>
            <td rowspan="2">Десятки</td>
            <td colspan="10">Единицы</td>
        </tr>
        <tr>
            <td>0</td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
            <td>5</td>
            <td>6</td>
            <td>7</td>
            <td>8</td>
            <td>9</td>
        </tr>
<?php
   for($a=0; $a<=9; $a++){
       echo '<tr>';
       echo "<td>$a</td>";
       for($b=0; $b<=9; $b++){
           $result = pow(($a.$b),2);
           echo "<td>$result</td>";
       }
       echo '</tr>';
   }
?>
    </table>
</body>
</html>
