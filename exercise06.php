<?php

$bmw = array('model'=>'X5', 'speed'=>120, 'doors'=>5, 'year'=> '2015');
$toyota = array('model'=>'Camry', 'speed'=>150, 'doors'=>5, 'year'=> '2013');
$opel = array('model'=>'Corsa', 'speed'=>140, 'doors'=>5, 'year'=> '2014');

foreach($bmw as $key=>$value) {
    if ($key == 'model') { echo $value; }
    else  echo " - $value";
}

echo '<br>';

foreach($toyota as $key=>$value) {
    if ($key == 'model') { echo $value; }
    else  echo " - $value";
}

echo '<br>';

foreach($opel as $key=>$value) {
    if ($key == 'model') { echo $value; }
    else  echo " - $value";
}

echo '<br>';
/* Задача. Создайте массив $moscow с ячейками name, population, area, density. Заполните ячейки значениями соответственно: "Москва", 12 325 387, 2561.5, 4811.78.
    Создайте массивы $saint_petersburg и $yekaterinburg налогичные массиву $moscow, заполните данными.
    Выведите значения всех трех массивов в виде: Город name. Население составлет population человек. Площадь города равна area кв.км. Плотность населения - density чел/кв.км.*/
$moscow = array('name'=>'Москва', 'population'=>12325387, 'area'=>2561.5, 'density'=>4811.78);
$saint_petersburg = array('name'=>'Санкт-Петербург', 'population'=>5222347, 'area'=>1439, 'density'=>3629.15);
$yekaterinburg = array('name'=>'Екатеринбург', 'population'=>1428042, 'area'=>1439, 'density'=>3629.15);

foreach($moscow as $key=>$value) {
    switch ($key) {
        case 'name':
            echo "Город $value. ";
            break;
        case 'population':
            echo "Население составляет $value человек. ";
            break;
        case 'area':
            echo "Площадь города равна $value кв.км. ";
            break;
        case 'density':
            echo "Плотность населения - $value чел/кв.км. ";
            break;
    }
}
echo '<br>';
foreach($saint_petersburg as $key=>$value) {
    switch ($key) {
        case 'name':
            echo "Город $value. ";
            break;
        case 'population':
            echo "Население составляет $value человек. ";
            break;
        case 'area':
            echo "Площадь города равна $value кв.км. ";
            break;
        case 'density':
            echo "Плотность населения - $value чел/кв.км. ";
            break;
    }
}
echo '<br>';
foreach($yekaterinburg as $key=>$value) {
    switch ($key) {
        case 'name':
            echo "Город $value. ";
            break;
        case 'population':
            echo "Население составляет $value человек. ";
            break;
        case 'area':
            echo "Площадь города равна $value кв.км. ";
            break;
        case 'density':
            echo "Плотность населения - $value чел/кв.км. ";
            break;
    }
}