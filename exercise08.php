<!doctype html>
<html>
<head>
    <style>
        ul {
            padding: 0;
            margin: 20px;
            list-style: none;
        }
    </style>
    <meta charset="UTF-8">
    <title>Exercise 08</title>
</head>
<body>
<?php

$menu = array('Home'=>'index.php', 'About'=>'about.php', 'Contacts'=>'contacts.php');
echo '<ul>';
foreach($menu as $item=>$file) {
    echo "<li><a href=\"$file\">$item</a></li>";
}
echo '</ul>';
 /* Задача. Используя цикл foreach отрисуйте вертикальное меню с вложеными меню.*/
$cpu = array('Intel'=>'intel.php', 'AMD'=>'amd.php', 'Qualcomm'=>'qualcomm.php');
$videoadapters = array('Nvidia'=>'nvidia.php', 'Radeon'=>'radeon.php');
$hdd = array('Seagate'=>'seagate.php', 'Western Digital'=>'westerndigital.php');
$components = array('CPU'=>$cpu, 'Videoadapters'=> $videoadapters, 'HDD'=>$hdd);
echo '<ul>';
foreach($components as $item=>$links) {
    echo "<li>$item<ul>";
    foreach($links as $name=>$file) {
        echo "<li><a href=\"$file\">$name</a></li>";
    }
    echo '</ul></li>';
}
echo '</ul>';
?>

</body>
</html>

