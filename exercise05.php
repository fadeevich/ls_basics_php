<?php
$day = 4;
switch ($day) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
        echo "Это рабочий день";
        break;
    case 6:
    case 7:
        echo "Это выходной день";
        break;
    default:
        echo "Неизвестный день";
}
echo '<br>';
/* Задача. Создайте переменную $year и присвойте ей произвольное числовое значение года.
    С помощью конструкции switch определите является ли год високосным (см. Википедию)
    Если год високоный выведите фразу "Это високосный год". если год не високосный - "Это не високосный год".*/
$year = 2568;
switch ($year % 4 == 0 and $year % 100 != 0 or $year % 400 == 0) {
    case true:
        echo "Это високосный год";
        break;
    case false:
        echo "Это не високосный год";
        break;
}